#! /Library/Frameworks/EPD64.framework/Versions/Current/bin/ipython

from pylab import *
import time
import math

#Work Piece Paramters

x_size = 2.263 #(Inches)
y_size = 0.7250 #(Inches)
#z_depth = 0.112 # -- Temporary shorter-distance for debugging 
z_depth = 1.239 #(Inches)
wirethick = 0.001 #(Inches)
x_inside = 1.758/2.0 #(Inches)
y_inside = 0.725/2.0 #(Inches)
z_start =  wirethick * math.pi # Offset 
z_step = wirethick * ( 1 + math.pi)

# Determine the angle where there is z-translation
# We rounded down to try and minizie the error on the translation
theta = math.floor(math.degrees(math.atan( y_inside / x_inside ))) 
# The complimentary angle of theta
longangle = 360.0-2.0*theta # Angle with no z-translation
a_step = theta

print theta,longangle

spindlespeed = 0 #(SHOULD NOT ROTATE!)
feedfast = 10.0 #(Inches/Minute)
feedslow = 1.0 #(Inches/Minute)

class GCodeWriter:
	def __init__(self):
		self.linenumber=1
	def append(self,text,prefix=True):
		if prefix:
			print "%s %s"%(self.prefix(),text)
			self.linenumber += 1
		else:
			print text
	def prefix(self):
		return "N%d0"%(self.linenumber)

g=GCodeWriter()

####################### HEADER #####################

g.append("%",False) 	    # Required to indicate start of file 
g.append("OProject",False)  # Sprut Cam Includes this so I will as well...
g.append("",False)

g.append("(DATE: %s)"%time.strftime("%d/%m/%Y"),False)
g.append("(TIME: %s)"%time.strftime("%H:%M:%S"),False)
g.append("",False)

############# Description of Tool Path ###############
g.append("(Wire winding: Parrallel Grid)",False)
g.append("(Z-translation along ~50deg angle)",False)
g.append("(Full Z-translation: %.3f)"%z_depth,False)

g.append("(Rotary machining)",False)
g.append("(STOCK/SQUARE, x:%.3f, y:%.3f, z:%.3f)"%(x_size,y_size,z_depth),False)
g.append("",False)


############### TOOL Parameters #####################

g.append("G20 (Inch)") #G20 = Define Units as Inch
g.append("G90 G64 G80 G40 G49 G18 G50")  

# G90 = Distance mode set to ABSOLUTE Values (G91=INCREMENTAL increase)
# G64 = Default Cutting Mode -- Constant Velocity
# G80 = Cancel Canned cycle - G0,G1,G2,or G3 mode must be re-established on the next move
# G49 = Tool length offset compensation cancel
# G40 = Tool radius compensation off
# G18 = Defines Plane Selection to XZ-Plane (Other Options G17-XY, G19-YZ)
# G50 = Resets all axis scale factors to 1.0

g.append("",False)
g.append("G93 (Feed = Units/minute)")
g.append("(TOOL/MILL,0.125,0.0,1.000,0.0)",False)
g.append("",False)

# M998 will intiate a move to tool change position (requires machine to be homed)

g.append("M05 (Stop Spindle)")     # M05 = Stop Spindel 
g.append("T10 G43 H0 M06")  

# T10 = Tool number 10 (Randomly chosen for now)
# G43 = Tool Height offset compensation
# H0 = Height offset value of 0
# M06 = Automatic Tool Change ---> T2 = Tool Number

g.append("S%d M09"%spindlespeed) #Coolant off (M09) and Spindel speed set to 0 (S0)
g.append("",False)

############### TOOL PATH  ###################

g.append("(Begin Winding)",False)

g.append("G1 Z%.6f A%.1f F%.1f"%(z_start,theta,feedslow)) #First Move Must Set G-value

FinalAngle = 360.0*(z_depth/z_step-1)

g.append("Z%.4f A%.1f F%.1f"%((z_depth),FinalAngle,feedslow))

g.append("M30")  # M30 = End Program
g.append("%",False)


