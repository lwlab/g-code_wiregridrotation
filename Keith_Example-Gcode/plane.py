#! /Library/Frameworks/EPD64.framework/Versions/Current/bin/ipython

from pylab import *

#work size (inches)
x_size=6
y_size=6
z_depth=0.01
y_step=1./8
spindle=10000

print("%")
print("(STOCK/BLOCK, "+str(x_size)+", "+str(y_size)+", "+str(z_depth)+", 0,0,"+str(z_depth)+")")
print("G20G64G17")
print("G90")
print("(TOOL/MILL,0.250,0.0,1.000,0.0)")
print("M6 T2")
print("M3 S"+str(spindle))
print("G0X0Y0Z0.1")
print("(Parallel Rough)")
print("(Roughling Level Depth: "+str(y_step)+")")
print("Z-"+str(z_depth))

for y in linspace(0,y_size,num=y_size/y_step/2-1):
	print("X"+str(x_size))
	print("Y"+str(y+y_step))
	print("X"+str(0))
	print("Y"+str(y+2*y_step))

print("M5")
print("M30")
print("(END)")
print("(OF PROGRAM)")

