#Wire Grid Rotation G-Code

##Subfolders:
###Mark_example-Gcode:
This folder has two example .TAP output files which include A-axis motions. These output files were used to construct the proper 
'*.TAP' file format, which includes the header.
###Keith_example-Gcode: 
This is a simple Python script that generates a tool path for cutting out a simple block. This code does not contain any A-axis 
movements - Code is not commented.


##Main Routines: 
The following two routines are fairly well documented for what the actual G-commands do and some of the other variants that might 
be using in for the purpose of this application. Two websites to consult for the actual G-code descriptions and command list are:

- http://en.wikipedia.org/wiki/G-code
- http://www.tormach.com/machine_codes_gcodes.html

Also, g-code is an umbrella term and would also include G-commands, M-commands, (T,S,F and other commands)
The main file extension for the CNC is a *.TAP file.

####Wrap_FullGrid.py:
To Run with Output: 

	$python Wrap_FullGrid.py >> Full_wrap.TAP

This is the main routine to produce the wire warping TAP file. This code produces a *.TAP file that is a tool path executed by CNC.
This code uses a defined wire grid size, wire thickness, and then determines an appropriate tool path that has parallel wire 
spacing. This is accomplished by doing a z-axis translation over a small angle (~45deg) then continuing the full rotation with a 
set z-hieght. 

####Wrap_AngelFeedrate.py:
To Run with Output: 

	$python Wrap_AngleFeedrate.py >> Feed_Angle.TAP

This was written to determine the FEEDRATES for both a-axis and z-axis. This is done by setting one feedrate value and observing 
what the a-axis rotation rate was and how the z-axis behaves. The final FEEDRATE value is dependent on multiple values thus is 
not necessarily a direct translation to rotation rate.

####Wrap_FullGrid_contin.py:
This program was written as a back-up in case there were issues with the way the Wrap_FullGrid.py operated. This code simply does 
the z-translation at the same time as a 360deg rotation in a-axis.


