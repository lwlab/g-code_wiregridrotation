from numpy import *
import time
import math

#workpiece parameters
ylen = 2.515
xwid = 0.510
y_trans = (ylen/2-xwid/2) #used as a constant
y_step = y_trans #will act as variable

#With y/a-axis zeroed such that the wire is perpendicular with the side wall: W.P. |___ G.N.
#In order to keep wire feed straight, W.P. should translate such that the wire is always in contact
#at the same location in the x-y plane

wirethick = 0.001 #(Inches) 

a_step = 0.00 #initial rotation to bring workpiece to first grab point
a_small = 70.00 #the angle of a-axis rotation for which a z-translation will occur
a_large = 290.00 #the angle of a-axis rotation for which wire is wound horizontally

#spindle z-zero set to just below window
z_length = 2.1 #wire will be set ~.05" before window 
z_step = wirethick * (1 + math.pi)


spindlespeed = 0 #(SHOULD NOT ROTATE!)
a_rate= 10.0 #(Inches/Minute); rotation rate remains contstant throughout winding

class GCodeWriter:
	def __init__(self):
		self.linenumber=1
	def append(self,text,prefix=True): #prints line and increments line number
		if prefix:
			print( "%s %s"%(self.prefix(),text) )
			self.linenumber += 1
		else:
			print( text )
	def prefix(self):
		return "N%d0"%(self.linenumber)

g=GCodeWriter()

# the Header, Desc. of Tool Path and Tool Parameters sections sets up the beginning portion of the postprcessor code

####################### HEADER #####################

g.append("%",False) 	    # Required to indicate start of file 
g.append("OProject",False)  # Sprut Cam Includes this so I will as well...
g.append("",False)

g.append("(DATE: %s)"%time.strftime("%d/%m/%Y"),False)
g.append("(TIME: %s)"%time.strftime("%H:%M:%S"),False)
g.append("",False)

############# Description of Tool Path ###############
g.append("(Wire winding: Parallel Grid)",False)
g.append("(Z-translation along ~70deg angle)",False)
g.append("(Full Z-translation: %.3f)"%z_length,False)

g.append("(Rotary machining)",False)
#g.append("(STOCK/SQUARE, x:%.3f, y:%.3f, z:%.3f)"%(x_size,y_size,z_depth),False)
g.append("",False)


############### TOOL Parameters #####################

g.append("G20 (Inch)") #G20 = Define Units as Inch
g.append("G90 G64 G80 G40 G49 G18 G50")  

# G90 = Distance mode set to ABSOLUTE Values (G91=INCREMENTAL increase)
# G64 = Default Cutting Mode -- Constant Velocity
# G80 = Cancel Canned cycle - G0,G1,G2,or G3 mode must be re-established on the next move
# G49 = Tool length offset compensation cancel
# G40 = Tool radius compensation off
# G18 = Defines Plane Selection to XZ-Plane (Other Options G17-XY, G19-YZ)
# G50 = Resets all axis scale factors to 1.0

g.append("",False)
g.append("G93 (Feed = Units/minute)")
g.append("(TOOL/MILL,0.125,0.0,1.000,0.0)",False)
g.append("",False)

# M998 will intiate a move to tool change positi.on (requires machine to be homed)

g.append("M05 (Stop Spindle)")     # M05 = Stop Spindle 
g.append("T10 G43 H0 M06")  

# T10 = Tool number 10 (Randomly chosen for now)
# G43 = Tool Height offset compensation
# H0 = Height offset value of 0
# M06 = Automatic Tool Change ---> T2 = Tool Number
 
g.append("S%d M09"%spindlespeed) #Coolant off (M09) and Spindle speed set to 0 (S0)
g.append("",False)

############### TOOL PATH  ###################

#-ytrans: base moves away from user; +ytans: base moves toward user
g.append("(Begin Winding)", False)

#rotate first 100 degrees
#y=0
#full y translation = every 90 degrees
a_step+=90.0
y_step=(-1.0)*y_step #translate -y full length to catch wire
g.append("G1 A%.1f Y%.4f F%.1f"%(a_step, y_step, a_rate)) #set initial G; move to first grab point
y_step+=(10.0/90.0)*y_trans #+y translation corresponds to angle of rotation now: 10/90
a_step+=10.0
g.append("G1 A%.1f Y%.4f F%.1f"%(a_step, y_step, a_rate))


for i in linspace(0, z_length, num = z_length/z_step-1):

	#rotation and translation for 70 deg
	y_step+=(a_small/90)*y_trans #+y translation still within previous 90 deg segment now: 80/90
	a_step+=a_small #increment a-axis angle
	g.append("Z%.4f A%.1f Y%.4f F%.1f"%((i+z_step),a_step, y_step, a_rate)) #print line for rotate + z-trans


	#rotation and translation for 290 degrees; break up into 90 degree increments
	a_step+=10.0
	y_step+=(10.0/90.0)*y_trans #+y translation still within previous 90 deg segment now: 90/90 
	g.append("A%.1f Y%.4f F%.1f"%(a_step, y_step, a_rate)) #print line for rotate + z-trans
	a_step+=90.0
	y_step=(-1.0)*y_trans #translate -y full length to catch wire
	g.append("A%.1f Y%.4f F%.1f"%(a_step, y_step, a_rate)) 
	a_step+=90.0
	g.append("A%.1f Y%.4f F%.1f"%(a_step, 0.0, a_rate)) #translate +y full length to catch wire
	a_step+=90.0
	g.append("A%.1f Y%.4f F%.1f"%(a_step, y_step, a_rate)) #translate -y full length to catch wire
	a_step+=10.0
	y_step+=(10.0/90.0)*y_trans #+y translation corresponds to angle of rotation now: 10/90
	g.append("A%.1f Y%.4f F%.1f"%(a_step, y_step, a_rate))

g.append("M30")  # M30 = End Program
g.append("%",False)

#python Wrap_FullFrid_V2.py>Wrap_FullGrid_V2.TAP   <--run this line to produce the TAP file







